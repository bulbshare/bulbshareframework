//
//  APMediaPreviewViewController.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 19/07/2017.
//  Copyright © 2017 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "APSingleBulbsharePagedView.h"

#import "APGradientView.h"

@class APMediaPreviewViewController;
@protocol APMediaPreviewViewControllerDelegate <NSObject>

- (void)startUpload;

@end

@interface APMediaPreviewViewController : UIViewController

@property (weak, nonatomic) IBOutlet APSingleBulbsharePagedView *pagedView;
@property (weak, nonatomic) NSMutableDictionary *bulbshareData;
@property (weak, nonatomic) BaseBulbshare *passingObject;
@property (weak, nonatomic) IBOutlet UIView *topView;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak) id<APMediaPreviewViewControllerDelegate> delegate;

@end
