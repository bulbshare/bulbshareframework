//
//  APSingleBulbsharePagedView.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 2. 09. 15.
//  Copyright (c) 2015 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreMedia/CMTime.h>
#import <Foundation/Foundation.h>
#import "CircleProgressBar.h"
#import "PBJVideoPlayerController.h"

#import "Brief.h"
#import "BaseBulbshare.h"

@class AVPlayer;
@class AVPlayerDemoPlaybackView;

@protocol APSingleBulbsharePagedViewDelegate <NSObject>
- (void)showReactions;
@end

@interface APSingleBulbsharePagedView : UIView <UIScrollViewDelegate, UITextViewDelegate>

@property(weak) id<APSingleBulbsharePagedViewDelegate> delegate;

@property (strong, nonatomic) UIScrollView *scrollView;
@property (strong, nonatomic) CircleProgressBar *progressBar;
@property (strong, nonatomic) CircleProgressBar *videoProgressBar;
@property (strong, nonatomic) UIButton *reloadImageButton;
@property (strong, nonatomic) UIButton *reloadVideoButton;
@property (nonatomic, strong) UIButton *reactionsButton;
@property (nonatomic) BOOL isSingleBulbshareScreen;
/*
 Views sub-properties
 */

// CellImage
@property (strong, nonatomic) UIImageView* cellImageView;
@property (strong, nonatomic) id passingObject;
@property (nonatomic) int previewOption; // 0 = for feed, 1 = for brief, 2 = bulbshare feed, 3 = preview
@property (nonatomic) BOOL isVideoLocal; // 0 no , 1 yes
@property (nonatomic) PBJVideoPlayerController *videoPlayerController;

- (void)resetVideoPlayer;
- (void)pauseVideoPlayer;
- (void)resetViews;
- (void)setDataForObject:(id)object onIndex:(int)index;
- (void)initViewWithMode:(int)mode;
- (BOOL)isPlaying;
- (void)rotateVideoToCurrentOrientation:(int)orientation;
- (UIImage*)rotateImageToCurrentOrientation:(UIImage*)image;
@end

