//
//  Person.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 11. 08. 15.
//  Copyright (c) 2015 Aventa Plus d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasePerson.h"

@interface Person : BasePerson

- (instancetype)init;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *displayName;
@property (nonatomic, strong) NSString *pictureURL;
@property (nonatomic, strong) NSString *bannerURL;
@property (nonatomic, strong) NSString *email;
@property (nonatomic, strong) NSString *gender;
@property (nonatomic, strong) NSString *birthday;
@property BOOL is_followed_by_me;
@property (nonatomic, strong) NSString *currentBrandTheme;

@property (nonatomic, strong) NSString *firstName;
@property (nonatomic, strong) NSString *lastName;
@property (nonatomic, strong) NSString *phone_number;
@property (nonatomic, strong) NSString *country_id;
@property (nonatomic, strong) NSString *country_name;
@property (nonatomic, strong) NSString *city_id;
@property (nonatomic, strong) NSString *city_name;
@property (nonatomic, strong) NSString *bio;

//Details
@property BOOL isVerified;
@property (nonatomic, strong) NSDate *lastModified;
@property (nonatomic, strong) NSDate *registeredOn;

//fb related
@property (nonatomic, strong) NSString *fbUserID;
@property NSInteger loginID;

// Other
@property NSInteger bulbsharesCount;
@property NSInteger user_role;

//Arrays
@property (nonatomic, strong) NSMutableArray* submitedBulbshares;
@property (nonatomic, strong) NSMutableArray* followedBrands;
@property (nonatomic, strong) NSMutableArray* followedFriends;
@property (nonatomic, strong) NSMutableArray* bulbshareArray;
@property (nonatomic, strong) NSMutableDictionary* socialAccounts;
@property (nonatomic, strong) NSString *leftImageURL;
@property (nonatomic, strong) NSString *middleImageURL;
@property (nonatomic, strong) NSString *rightImageURL;

@end
