//
//  BaseBulbshare.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 2. 09. 15.
//  Copyright (c) 2015 Aventa Plus d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "Person.h"
#import "Brief.h"

@interface BaseBulbshare : NSObject

@property (strong, nonatomic) NSString *bulbshareId;
@property (strong, nonatomic) NSString *parentBriefref;
@property int parentBriefid;
@property (strong, nonatomic) NSString *parentBriefTitle;
@property (strong, nonatomic) NSString *bulbshareref;
@property (strong, nonatomic) NSString *comment;
@property (strong, nonatomic) NSString *title;
@property (strong, nonatomic) NSString *sherableUrl;
@property (strong, nonatomic) NSString *s3PresignedUrl;
@property (strong, nonatomic) NSString *s3Key;
@property (strong, nonatomic) NSString *s3VideoTnPresignedUrl;
@property (strong, nonatomic) NSString *s3VideoTnKey;
@property (strong, nonatomic) NSString *s3ImagePresignedUrl;
@property (strong, nonatomic) NSString *s3ImageKey;
@property int comments_count;
@property int likes_count;
@property int uploadStatus; //0 = ready to start first upload, 1 = upload in progress, 2 = upload completed, 3 = basic upload failed, 4 = long video upload failed, 5 = image upload failed, 6 = short video failed, 7 = resend S3 url failed
@property int briefType; //0 = short video, 1 = long video, , 2 = bulbshare media s3 video
@property int uploadCount;
@property NSDate *lastUploadDate;
@property BOOL is_activated;
@property (nonatomic, strong) NSString *picture;
@property (nonatomic, strong) NSString *thumbnailPicture;
@property (nonatomic, strong) NSString *video;
@property (nonatomic, strong) NSData *videoTn;
@property (nonatomic, strong) NSString *localVideo;
@property (nonatomic, strong) Person* user;
@property (nonatomic, strong) Brief* brief;
@property (nonatomic, strong) NSDate *submitted_on;
@property (nonatomic, strong) NSData *font_color_data;
@property (nonatomic, strong) NSData *bg_color_data;
@property (nonatomic, strong) NSData *local_image_data;
@property NSInteger text_align;
@property BOOL was_liked_by_me;
@property BOOL is_shareable;
@property (nonatomic, strong) NSString *brandname;
@property BOOL is_private;
@property BOOL is_imageLocal;
@property NSInteger brandid;

@end
