#import <UIKit/UIKit.h>

@interface APHorizontalLineProgressView : UIView

@property (nonatomic) NSMutableArray *segments;
@property (nonatomic,assign) CGFloat  barThickness;
@property (nonatomic,assign) CGFloat  progressValue;
@property (nonatomic,assign) CGFloat  segmentValue;

- (void)initalSetup;
- (void)resetProgressValue;

@end
