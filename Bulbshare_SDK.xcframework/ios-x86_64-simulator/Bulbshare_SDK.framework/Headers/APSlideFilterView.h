//
//  APSlideFilterView.h
//  APSlideFilter
//
//  Created by Nghia Tran on 6/11/14.
//  Copyright (c) 2014 Fe. All rights reserved.
//

#import <UIKit/UIKit.h>

// Protocol
@protocol APSlideFilterViewDataSource;
@protocol APSlideFilterViewDelegate;

@interface APSlideFilterView : UIView
// Scroll View
@property (strong, nonatomic) UIScrollView *scrollView;

// Delegate
@property (weak, nonatomic) id<APSlideFilterViewDelegate> delegate;

// Data Source
@property (weak, nonatomic) id<APSlideFilterViewDataSource> dataSource;

// Number of filter
@property (assign, readonly, nonatomic) NSInteger numberOfFilter;

// Current index
@property (assign,readonly, nonatomic) NSInteger currentIndex;

//Done Btn
@property (strong, nonatomic) UIButton *doneBtn;

// Reload
-(void) reloadFilter;

// Refresh View
-(void) refreshView;
-(void) resetView;
-(void) removeTitles;
-(void) configureLayer;
-(void) configureLayerForIndex:(int)index;
@end

////////////////
// Protocol - Data source
@protocol APSlideFilterViewDataSource <NSObject>

@required
-(NSInteger) numberOfFilter;
-(NSString *) APSlideFilterView:(APSlideFilterView *) sender titleFilterAtIndex:(NSInteger) index;
-(UIImage *) APSlideFilterView:(APSlideFilterView *) sender imageFilterAtIndex:(NSInteger) index;

@optional
-(UIFont *) APSlideFilterView:(APSlideFilterView *) sender fontForTitleAtIndex:(NSInteger) index;
-(NSString *) kCAContentGravityForLayer;
@end

//////////////
// Protocol - Delegate
@protocol APSlideFilterViewDelegate <NSObject>
@optional
-(void) APSlideFilterView:(APSlideFilterView *) sender didTapDoneButtonAtIndex:(NSInteger) index;
-(BOOL) APSlideFilterView:(APSlideFilterView *)sender shouldSlideFilterAtIndex:(NSInteger) index;
-(void) APSlideFilterView:(APSlideFilterView *)sender didBeginSlideFilterAtIndex:(NSInteger) index;
-(void) APSlideFilterView:(APSlideFilterView *)sender didEndSlideFilterAtIndex:(NSInteger) index;
@end


