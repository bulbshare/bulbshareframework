#import <UIKit/UIKit.h>

@class APSegmentedControl;

typedef void (^IndexChangeBlock)(NSInteger index);
typedef NSAttributedString *(^APTitleFormatterBlock)(APSegmentedControl *segmentedControl, NSString *title, NSUInteger index, BOOL selected);

enum {
    APSegmentedControlNoSegment = -1   // Segment index for no selected segment
};

@interface APSegmentedControl : UIControl

@property (nonatomic, strong) NSArray<NSString *> *sectionTitles;
@property (nonatomic, copy) IndexChangeBlock indexChangeBlock;

@property (nonatomic, strong) UIColor *backgroundColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *selectionIndicatorColor UI_APPEARANCE_SELECTOR;
@property (nonatomic, strong) UIColor *selectionIndicatorBoxColor UI_APPEARANCE_SELECTOR;

@property(nonatomic, getter = isUserDraggable) BOOL userDraggable;
@property(nonatomic, getter = isTouchEnabled) BOOL touchEnabled;

@property (nonatomic, assign) NSInteger selectedSegmentIndex;
@property (nonatomic, readwrite) CGFloat selectionIndicatorHeight;
@property (nonatomic, readwrite) UIEdgeInsets selectionIndicatorEdgeInsets;
@property (nonatomic, readwrite) UIEdgeInsets segmentEdgeInset;
@property (nonatomic, readwrite) UIEdgeInsets enlargeEdgeInset;

@property (nonatomic) BOOL shouldAnimateUserSelection;

- (id)initWithSectionTitles:(NSArray<NSString *> *)sectiontitles;
- (void)setSelectedSegmentIndex:(NSUInteger)index animated:(BOOL)animated;

@end

