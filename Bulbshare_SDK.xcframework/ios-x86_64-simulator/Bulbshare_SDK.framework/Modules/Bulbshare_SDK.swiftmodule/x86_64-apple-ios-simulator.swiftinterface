// swift-interface-format-version: 1.0
// swift-compiler-version: Apple Swift version 5.5.2 (swiftlang-1300.0.47.5 clang-1300.0.29.30)
// swift-module-flags: -target x86_64-apple-ios12.1-simulator -enable-objc-interop -enable-library-evolution -swift-version 5 -enforce-exclusivity=checked -O -module-name Bulbshare_SDK
import AVFoundation
import AVKit
import Alamofire
@_exported import Bulbshare_SDK
import CommonCrypto
import CoreAudio
import CoreData
import FBSDKCoreKit
import FBSDKLoginKit
import FBSDKShareKit
import Foundation
import GoogleSignIn
import ImageIO
import KeychainAccess
import MBProgressHUD
import Swift
import SwiftUI
import SystemConfiguration
import TwitterKit
import UIKit
import WebKit
import _Concurrency
@_inheritsConvenienceInitializers @objc public class Localization : ObjectiveC.NSObject {
  @objc public static let submitCloseTitleText: Swift.String
  @objc public static let createBulbshare: Swift.String
  @objc public static let enterTitleHere: Swift.String
  @objc public static let enterTextHere: Swift.String
  @objc public static let closeSubmitText: Swift.String
  @objc public static let storeSubmitText: Swift.String
  @objc public static let submitCloseMessageText: Swift.String
  @objc public static let downloadingVideo: Swift.String
  @objc public static let exportingVideo: Swift.String
  @objc public static let addTitle: Swift.String
  @objc public static let unableToImportVideo: Swift.String
  @objc public static let unableToImportImage: Swift.String
  @objc public static let createYourBulbshareLongVideo: Swift.String
  @objc public static let tapToTakePhoto: Swift.String
  @objc public static let videoLengthLimitTitle: Swift.String
  @objc public static let videoLengthLimitMessage: Swift.String
  @objc override dynamic public init()
  @objc deinit
}
extension UIKit.UIImageView {
  @_Concurrency.MainActor(unsafe) public func loadGif(name: Swift.String)
}
extension UIKit.UIImage {
  public class func gif(data: Foundation.Data) -> UIKit.UIImage?
  public class func gif(url: Swift.String) -> UIKit.UIImage?
  public class func gif(name: Swift.String) -> UIKit.UIImage?
}
public struct BSUserRefInput : Swift.Encodable {
  public init(userRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSBulbshareListInput : Swift.Encodable {
  public init(briefRef: Swift.String, count: Swift.Int, sortType: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSSingleBulbshareInput : Swift.Encodable {
  public init(bulbshareRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
@_inheritsConvenienceInitializers @objc(CDPollBrief) public class CDPollBrief : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
@objc public class BriefResponseData : ObjectiveC.NSObject, Swift.Codable {
  @objc deinit
  public func encode(to encoder: Swift.Encoder) throws
  required public init(from decoder: Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class BSSDKConfig : ObjectiveC.NSObject {
  public init(appVersion: Swift.String, appName: Swift.String, appId: Swift.String, apiKey: Swift.String, wlaKey: Swift.String)
  @objc override dynamic public init()
  @objc deinit
}
extension Bulbshare_SDK.SDKError : Foundation.LocalizedError {
  public var errorDescription: Swift.String? {
    get
  }
  public var failureReason: Swift.String? {
    get
  }
}
public enum SDKError : Swift.Error {
  public enum ApplicationError : Swift.String, Swift.Error {
    case unknownError
    case unauthorized
    case deviceIsJailBreak
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum NetworkError : Swift.String, Swift.Error {
    case noInternetConnection
    case connectionTimeOut
    case missingUrl
    case unknownError
    case internalServerError
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum ServerError : Swift.String, Swift.Error {
    case unAuthorized
    case unknownError
    case userDoesNotExist
    case invalidPassword
    case invalidArguments
    case invalidBrief
    case briefNotExist
    case briefIsRemoved
    case invalidBriefType
    case invalidComment
    case invalidBriefCommentReference
    case briefCommentNotExist
    case briefCommentRemoved
    case invalidPermissions
    case invalidItemCount
    case invalidPage
    case invalidReturnStatusValue
    case brandNotExist
    case brandRemoved
    case pollNotExist
    case invalidMediaFile
    case invalidMediaType
    case invalidPollitemID
    case failedToUploadUrl
    case invalidPollitem
    case accessDenied
    case blockedAppVersion
    case deviceRemoved
    case deviceMismatch
    case NoAuthenticationHeader
    case invalidUserReference
    case invalidToken
    case wrongUserReference
    case inactiveCredentials
    case wrongAuthSecret
    case userInactive
    case userRemoved
    case requestIdExpired
    case invalidRequest
    case wrongDeviceReference
    case invalidDeviceReference
    case responseLimitReached
    case invalidResponseValue
    case noPollAvailable
    case invalidChecksumInput
    case invalidKey
    case WrongAuthenticationSecret
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  public enum JSONError : Swift.String, Swift.Error {
    case jsonDecodeError
    public init?(rawValue: Swift.String)
    public typealias RawValue = Swift.String
    public var rawValue: Swift.String {
      get
    }
  }
  case applicationError(error: Bulbshare_SDK.SDKError.ApplicationError)
  case networkError(error: Bulbshare_SDK.SDKError.NetworkError)
  case serverError(reason: Bulbshare_SDK.SDKError.ServerError)
  case jsonError(reason: Bulbshare_SDK.SDKError.JSONError)
}
extension Bulbshare_SDK.CDPollBrief {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<Bulbshare_SDK.CDPollBrief>
  @objc @NSManaged dynamic public var isUploaded: Swift.Bool {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var ref: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var response: Foundation.NSSet? {
    @objc get
    @objc set
  }
}
extension Bulbshare_SDK.CDPollBrief {
  @objc(addResponseObject:) @NSManaged dynamic public func addToResponse(_ value: Bulbshare_SDK.CDPollResponse)
  @objc(removeResponseObject:) @NSManaged dynamic public func removeFromResponse(_ value: Bulbshare_SDK.CDPollResponse)
  @objc(addResponse:) @NSManaged dynamic public func addToResponse(_ values: Foundation.NSSet)
  @objc(removeResponse:) @NSManaged dynamic public func removeFromResponse(_ values: Foundation.NSSet)
}
public protocol VideoPickerDelegate : AnyObject {
  func didSelect(url: Foundation.URL?)
}
@objc open class VideoPicker : ObjectiveC.NSObject {
  public init(presentationController: UIKit.UIViewController, delegate: Bulbshare_SDK.VideoPickerDelegate)
  public func present(from sourceView: UIKit.UIView, sourceType: UIKit.UIImagePickerController.SourceType)
  @objc deinit
}
extension Bulbshare_SDK.VideoPicker : UIKit.UIImagePickerControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerController(_ picker: UIKit.UIImagePickerController, didFinishPickingMediaWithInfo info: [UIKit.UIImagePickerController.InfoKey : Any])
}
extension Bulbshare_SDK.VideoPicker : UIKit.UINavigationControllerDelegate {
}
extension Bulbshare_SDK.CDPollCollection {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<Bulbshare_SDK.CDPollCollection>
  @objc @NSManaged dynamic public var comment: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var mediaType: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var optionid: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var relX: Swift.Double {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var relY: Swift.Double {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var sentiment: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var value: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var video_tn: Swift.String? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var response: Bulbshare_SDK.CDPollResponse? {
    @objc get
    @objc set
  }
}
public struct BSLinkInput : Swift.Encodable {
  public init(type: Swift.Int, authentication: Bulbshare_SDK.LinkInputModel, data: Bulbshare_SDK.LinkInputModel)
  public func encode(to encoder: Swift.Encoder) throws
}
@_inheritsConvenienceInitializers @objc public class BaseTheme : ObjectiveC.NSObject {
  @objc public static var theme: Bulbshare_SDK.BaseTheme
  @objc public var channelId: Swift.Int
  @objc public func setSubmitBulbshareButtonColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setSubmitBulbshareButtonTextColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setLastIconImageTrayColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setMediaLibraryTopPartColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setCloseIconOnImageVideoColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setSubmitProgressColor(channelId: Swift.Int) -> UIKit.UIColor
  @objc public func setCommentColor()
  @objc override dynamic public init()
  @objc deinit
}
@_hasMissingDesignatedInitializers public class BulbshareClient {
  public static func shared(config: Bulbshare_SDK.BSSDKConfig) -> Bulbshare_SDK.BulbshareClient
  weak public var authenticationDelegate: Bulbshare_SDK.BSAuthenticatioinDelegate?
  public func authenticate(input: Bulbshare_SDK.BSAuthenticationInput)
  public func setTheme(channelId: Swift.Int, theme: Bulbshare_SDK.BSSdkThemeInput)
  weak public var feedDelegate: Bulbshare_SDK.BSFeedDelegate?
  public func showFeed(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSBriefInput)
  weak public var pollBriefDelegate: Bulbshare_SDK.BSPollBriefDdelegate?
  public func showPollBrief(viewController: UIKit.UIViewController, input: Swift.String)
  weak public var briefDelegate: Bulbshare_SDK.BSBriefDelegate?
  public func showBrief(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSBriefInput)
  weak public var briefIntroDelegate: Bulbshare_SDK.BSBriefIntroDelegate?
  public func showBriefIntro(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSBriefRefInput)
  weak public var singleBulbshareDelegate: Bulbshare_SDK.BSSingleBulbshareDelegate?
  public func showSingleBulbshare(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSSingleBulbshareInput)
  weak public var bulbshareListDelegate: Bulbshare_SDK.BSBulbshareDelegate?
  public func showBulbshareList(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSBulbshareListInput)
  weak public var bulbshareFeedDelegate: Bulbshare_SDK.BSBulbshareFeedDelegate?
  public func showBulbshareFeed(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSBulbshareFeedInput)
  weak public var rewardsDelegate: Bulbshare_SDK.BSRewardsDelegate?
  public func showRewards(viewController: UIKit.UIViewController)
  weak public var commentDelegate: Bulbshare_SDK.BSCommentDelegate?
  public func showComment(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSBriefRefInput)
  weak public var createBulshareDelegate: Bulbshare_SDK.BSCreateSubmitDelegate?
  public func showcreateBulbshare(breifRef: Swift.String, viewController: UIKit.UIViewController, type: Swift.Int)
  public func handleUnSubmittedSurvey()
  weak public var myProfileDelegate: Bulbshare_SDK.BSMyProfileDelegate?
  public func showMyProfile(viewController: UIKit.UIViewController, input: Bulbshare_SDK.BSUserRefInput)
  @objc deinit
}
public struct LinkInputModel : Swift.Codable {
  public func encode(to encoder: Swift.Encoder) throws
  public init(from decoder: Swift.Decoder) throws
}
@_inheritsConvenienceInitializers @objc public class DeviceManager : ObjectiveC.NSObject {
  @objc public static let shared: Bulbshare_SDK.DeviceManager
  @objc public func device() -> Bulbshare_SDK.Device
  @objc override dynamic public init()
  @objc deinit
}
public protocol BSAuthenticatioinDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessAuthentication()
}
public protocol BSBriefDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSErrorDelegate : AnyObject {
  func onSDKError(error: Swift.Error)
}
public protocol BSBulbshareDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSBulbshareFeedDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessBriefLoaded()
}
public protocol BSRewardsDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessRewardsLoaded()
}
public protocol BSPollBriefDdelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessPollBriefLoaded()
}
public protocol BSBriefIntroDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessBriefIntroLoaded()
}
public protocol BSSingleBulbshareDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessSingleBulbshareLoaded()
}
public protocol BSCommentDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessCommentLoaded()
}
public protocol BSFeedDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessFeedLoaded()
}
public protocol BSCreateSubmitDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessCreateSubmitDelegate()
}
public protocol BSMyProfileDelegate : Bulbshare_SDK.BSErrorDelegate {
  func onSuccessMyProfileLoaded()
}
public struct BSBriefInput : Swift.Encodable {
  public init(privateBrandId: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
public struct BSBriefRefInput : Swift.Encodable {
  public init(briefRef: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
@_inheritsConvenienceInitializers @objc(CDPollResponse) public class CDPollResponse : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
public struct BSBulbshareFeedInput : Swift.Encodable {
  public init(privateBrandID: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
@_hasMissingDesignatedInitializers @objc public class Device : ObjectiveC.NSObject {
  @objc public var device_os: Swift.String!
  @objc deinit
}
@_inheritsConvenienceInitializers @objc(CDPollCollection) public class CDPollCollection : CoreData.NSManagedObject {
  @objc override dynamic public init(entity: CoreData.NSEntityDescription, insertInto context: CoreData.NSManagedObjectContext?)
  @objc deinit
}
@_inheritsConvenienceInitializers @objc public class KeychainAccessHandler : ObjectiveC.NSObject {
  @objc public func getUserSecret() -> Swift.String?
  @objc public func getUserRef() -> Swift.String?
  @objc public func getDeviceRef() -> Swift.String?
  @objc override dynamic public init()
  @objc deinit
}
extension Bulbshare_SDK.CDPollResponse {
  @nonobjc public class func fetchRequest() -> CoreData.NSFetchRequest<Bulbshare_SDK.CDPollResponse>
  @objc @NSManaged dynamic public var pollitemid: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var pollType: Swift.Int32 {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var collection: Foundation.NSSet? {
    @objc get
    @objc set
  }
  @objc @NSManaged dynamic public var pollRequest: Bulbshare_SDK.CDPollBrief? {
    @objc get
    @objc set
  }
}
extension Bulbshare_SDK.CDPollResponse {
  @objc(addCollectionObject:) @NSManaged dynamic public func addToCollection(_ value: Bulbshare_SDK.CDPollCollection)
  @objc(removeCollectionObject:) @NSManaged dynamic public func removeFromCollection(_ value: Bulbshare_SDK.CDPollCollection)
  @objc(addCollection:) @NSManaged dynamic public func addToCollection(_ values: Foundation.NSSet)
  @objc(removeCollection:) @NSManaged dynamic public func removeFromCollection(_ values: Foundation.NSSet)
}
@_inheritsConvenienceInitializers @objc public class SDKBundles : ObjectiveC.NSObject {
  @objc public class func bundle() -> Foundation.Bundle?
  @objc override dynamic public init()
  @objc deinit
}
public struct BSUnlinkInput : Swift.Encodable {
  public init(useraccountid: Swift.Int)
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol ImagePickerDelegate : AnyObject {
  func didSelect(image: UIKit.UIImage?)
}
@objc open class ImagePicker : ObjectiveC.NSObject {
  public init(presentationController: UIKit.UIViewController, delegate: Bulbshare_SDK.ImagePickerDelegate)
  public func pickWithoutEditing()
  public func presentImagePicker(from sourceView: UIKit.UIView, sourcetype: UIKit.UIImagePickerController.SourceType)
  @objc deinit
}
extension Bulbshare_SDK.ImagePicker : UIKit.UIImagePickerControllerDelegate {
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerControllerDidCancel(_ picker: UIKit.UIImagePickerController)
  @_Concurrency.MainActor(unsafe) @objc dynamic public func imagePickerController(_ picker: UIKit.UIImagePickerController, didFinishPickingMediaWithInfo info: [UIKit.UIImagePickerController.InfoKey : Any])
}
extension Bulbshare_SDK.ImagePicker : UIKit.UINavigationControllerDelegate {
}
public struct BSAuthenticationInput : Swift.Encodable {
  public init(email: Swift.String, identifier: Swift.String, accessToken: Swift.String, firstName: Swift.String, lastName: Swift.String, gender: Swift.String, birthDay: Swift.String, country: Swift.String)
  public func encode(to encoder: Swift.Encoder) throws
}
public protocol SDKSettingsProtocol {
  static func sdkVersion() -> Swift.String
}
@_hasMissingDesignatedInitializers public class SDKSettings : Bulbshare_SDK.SDKSettingsProtocol {
  public static func sdkVersion() -> Swift.String
  @objc deinit
}
public struct BSSdkThemeInput {
  public var loadingProgressColor: Bulbshare_SDK.ThemeColor?
  public var sendButtonColor: Bulbshare_SDK.ThemeColor?
  public var tabIndicatorColor: Bulbshare_SDK.ThemeColor?
  public var tabIndicatorTextColor: Bulbshare_SDK.ThemeColor?
  public var lazyLoadingSpinnerColor: Bulbshare_SDK.ThemeColor?
  public var activeTimeLeftColor: Bulbshare_SDK.ThemeColor?
  public var completedTimeLeftColor: Bulbshare_SDK.ThemeColor?
  public var shareButtonBgColor: Bulbshare_SDK.ThemeColor?
  public var myProfileBannerColor: Bulbshare_SDK.ThemeColor?
  public var socialMediaNameColor: Bulbshare_SDK.ThemeColor?
  public var imageHighlightBGColor: Bulbshare_SDK.ThemeColor?
  public var pollImageBgLogo: UIKit.UIImage?
  public init(primaryColor: Bulbshare_SDK.ThemeColor, secondaryColor: Bulbshare_SDK.ThemeColor)
}
public struct ThemeColor {
  public var r: Swift.Int, g: Swift.Int, b: Swift.Int, a: Swift.Int
  public init(r: Swift.Int, g: Swift.Int, b: Swift.Int, a: Swift.Int)
}
extension Bulbshare_SDK.SDKError.ApplicationError : Swift.Equatable {}
extension Bulbshare_SDK.SDKError.ApplicationError : Swift.Hashable {}
extension Bulbshare_SDK.SDKError.ApplicationError : Swift.RawRepresentable {}
extension Bulbshare_SDK.SDKError.ServerError : Swift.Equatable {}
extension Bulbshare_SDK.SDKError.ServerError : Swift.Hashable {}
extension Bulbshare_SDK.SDKError.ServerError : Swift.RawRepresentable {}
extension Bulbshare_SDK.SDKError.JSONError : Swift.Equatable {}
extension Bulbshare_SDK.SDKError.JSONError : Swift.Hashable {}
extension Bulbshare_SDK.SDKError.JSONError : Swift.RawRepresentable {}
extension Bulbshare_SDK.SDKError.NetworkError : Swift.Equatable {}
extension Bulbshare_SDK.SDKError.NetworkError : Swift.Hashable {}
extension Bulbshare_SDK.SDKError.NetworkError : Swift.RawRepresentable {}
