##Changelog


All notable changes to this project are documented in this file.


The changes is based on the following types:

* **Added** for new features.
* **Changed** for changes in existing functionality.
* **Deprecated** for soon-to-be removed features.
* **Removed** for now removed features.
* **Fixed** for any bug fixes.
* **Security** in case of vulnerabilities.
* **Unreleased** for future updates


And this project adheres to Semantic Versioning and the versioning is done with the format MAJOR.MINOR (eg:- v1.4) where

* **MAJOR** version: when you make incompatible changes and major features/functionality changes.
* **MINOR** version: when you add functionality with some fixes.


##[v1.1] - 2021-12-14


### **Added**
* Public functions for all modules
* Indicator/Loaders on Images
* Pagination on Briefs


### **Fixed**

* Error Handling in all the modules.
* Removed Hardcoded Strings
* Local images using extension
* Open text screen title issue
* Like,Comment Count on Brief Feed
* Next Button Generic for all poll types


##[v1.0] - 2021-12-06

### **Added**

Basic functionality:

* Authentication Module 
* Feed Module
* Brief Module
* Brief Intro Module
* Comment Module
* Poll Brief Module
* Like/Unlike Briefs


