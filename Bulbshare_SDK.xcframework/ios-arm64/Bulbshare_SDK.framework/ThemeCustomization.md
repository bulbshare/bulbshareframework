# Theme Customization
There are various theme customization available to the user of the SDK.

First create a object of **BSSdkThemeInput**

	var theme = BSSdkThemeInput.init(primaryColor: ThemeColor.init(r: 0, g: 0, b: 0, a: 1), secondaryColor: ThemeColor.init(r: 240, g: 20, b: 166, a: 1))
	
Now in the viewDidLoad call the various Customizable elements as mentioned below in the sheet

	theme.sendButtonColor = ThemeColor.init(r: 45, g: 242, b: 23, a: 1)
	
## Various Customization Available

### loading Progress Color

It is used to set the color of all the Loader in the SDK.

	theme.loadingProgressColor = ThemeColor.init(r: 255, g: 125, b: 253, a: 1)

### Send Button Color

It is used to set the color of Send button in comment screen in the SDK.

	theme.sendButtonColor = ThemeColor.init(r: 45, g: 242, b: 23, a: 1)

### Tab Indicator Color

It is used to set the color of Tab Indicator in the SDK.

	theme.tabIndicatorColor = ThemeColor.init(r: 255, g: 125, b: 253, a: 1)

### Tab Indicator Text Color

It is used to set the color of Tab Indicator Text Color in the SDK.

	theme.tabIndicatorTextColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)

### Lazy Loading Spinner Color

It is used to set the color of Lazy loading Spinner in the SDK.

	theme.lazyLoadingSpinnerColor = ThemeColor.init(r: 255, g: 125, b: 253, a: 1)

### Active Time Left Color

It is used to set the color of Time left of Brief in the SDK.

	theme.activeTimeLeftColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)
	
### Completed Time Left Color

It is used to set the color of completed Label on brief screen in the SDK.

	theme.completedTimeLeftColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)

### Share Button BgColor

It is used to set the color of Share Button in the SDK.

	theme.shareButtonBgColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)

### My Profile Banner Color

It is used to set the color of My profile Banner in the SDK.

	theme.myProfileBannerColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)	

### Social Media Name Color

It is used to set the color of Social Media Name in the SDK.

	theme.socialMediaNameColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)
	
### Image Highlight BGColor

It is used to set the color of Image Highlight Background color in the SDK.

	theme.imageHighlightBGColor = ThemeColor.init(r: 20, g: 30, b: 40, a: 1)
	
### Poll Image BGLogo

It is used to set the image of poll Brief Background in the SDK.

	theme.pollImageBgLogo = UIImage(named: "liked")