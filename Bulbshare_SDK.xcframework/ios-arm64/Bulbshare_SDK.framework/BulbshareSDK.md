# **Bulbshare SDK**

Bulbshare Library is an iOS Library which Collaborate with the causes you care about and
the brands you love from the palm of your hand. Follow your friends and be inspired by Bulbshares submitted from a community of
creative collaborators from across the globe.

Use Bulbshare to:

- connect with the brands and causes you care about
- share multiple images and video
- discover Bulbshares from your friends and followers
- access exclusive rewards and opportunities

## Compatibility

1. iOS 12+
2. Xcode 13.0+
3. Device orientation - portrait

## SDK Creation

### 1) Manual Creation
No steps are required

### 2) Automatic Creation
To create compiled sdk (**BulbshareSDK.xcframework**) follow the steps below:

1. Go to terminal in root of the project
2. Run below command to archive sdk for iOS devices
 
		xcodebuild archive -workspace BulbshareSDK.xcworkspace -scheme BulbshareSDK -destination "generic/platform=iOS" -archivePath ../output/BulbshareSDK-iOS SKIP_INSTALL=NO BUILD_LIBRARY_FOR_DISTRIBUTION=YES

3. Run below command to archive sdk for iOS Simulator
		
		xcodebuild archive -workspace BulbshareSDK.xcworkspace -scheme BulbshareSDK -destination "generic/platform=iOS Simulator" -archivePath ../output/BulbshareSDK-Sim SKIP_INSTALL=NO BUILD_LIBRARY_FOR_DISTRIBUTION=YES

4. Run below command to create final fat framework BulbshareSDK.xcframework which will be created in the output folder.

		xcodebuild -create-xcframework -framework  ../output/BulbshareSDK-iOS.xcarchive/Products/Library/Frameworks/BulbshareSDK.framework -framework  ../output/BulbshareSDK-Sim.xcarchive/Products/Library/Frameworks/BulbshareSDK.framework -output  ../output/BulbshareSDK.xcframework

## SDK Deployment

 There are two ways to deploy the BulbshareSDK

### 1) Manual Deployment

 
 A) Add the SDK dependencies in the podfile of the client Application
 
 	pod 'SDWebImage'
 	pod 'SwiftLint'
 	pod 'KeychainAccess'
 	pod 'IQKeyboardManagerSwift'
 	pod 'Koloda'
 	pod 'MBProgressHUD'
 	pod "XCDYouTubeKit", "~> 2.15"
 	pod 'SVProgressHUD'
 	pod 'JDStatusBarNotification'
 	pod 'KSToastView', '0.5.7'
 	pod 'SCRecorder'
 	pod 'TwitterKit'
 	pod 'FacebookCore'
 	pod 'FacebookLogin'
 	pod 'FacebookShare'
 	pod 'Alamofire'
 	pod 'GoogleSignIn', '~> 5.0'
 	pod 'Firebase/Core'
 	pod 'Firebase/Auth'
 	post_install do |installer|
           installer.pods_project.targets.each do |target|
             target.build_configurations.each do |config|
               config.build_settings['BUILD_LIBRARY_FOR_DISTRIBUTION'] = 'YES'
               config.build_settings['IPHONEOS_DEPLOYMENT_TARGET'] = '12.1'
                   config.build_settings["EXCLUDED_ARCHS[sdk=iphonesimulator*]"] = "arm64"

             end
           end
         end

	end
	
B) Now Add **BulbshareSDK.xcframework** which is being created in the **output folder** mentioned in the **SDK Creation** step to Application target
 
C) Go to project target and in general tab choose **BulbshareSDK.xcframework** in framework,libraries and embedded contents and change from **Do not embed** to **embed and sign**

![](images/image1.png)

### 2) Automatic Deployment
Add bulbshare pod in the podfile of the client application

	pod 'BulbshareSDK', git: 'https://bitbucket.org/bulbshare/bulbshareframework.git'
 


