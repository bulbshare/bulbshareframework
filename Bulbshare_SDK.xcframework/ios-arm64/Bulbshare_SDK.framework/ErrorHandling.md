# Bulbshare SDK 

##  Error Handling

**SDKError** throws Error object if any error occurs, you can check protocol method
 
 These are the registered errors list: 
 
	  SDKError.ServerError.unknownError.rawValue 
	  SDKError.ServerError.userDoesNotExist.rawValue 
	  SDKError.ServerError.invalidPassword.rawValue 
	  SDKError.ServerError.invalid_arguments.rawValue 
	  SDKError.ServerError.brief_not_exist.rawValue 
	  SDKError.ServerError.brief_is_removed.rawValue 
	  SDKError.ServerError.invalid_brief_type.rawValue 
	  SDKError.ServerError.invalidComment.rawValue 
	  SDKError.ServerError.invalidBriefCommentReference.rawValue 
	  SDKError.ServerError.briefCommentNotExist.rawValue 
	  SDKError.ServerError.briefCommentRemoved.rawValue 
	  SDKError.ServerError.invalidPermissions.rawValue 
	  SDKError.ServerError.invalidItemCount.rawValue 
	  SDKError.ServerError.invalidPage.rawValue   
	  SDKError.ServerError.invalidReturnStatusValue.rawValue
	  SDKError.ServerError.brandNotExist.rawValue
	  SDKError.ServerError.brandRemoved.rawValue
	  	  
	  
  
##ServerError Errors

|Error Code 							 | Description		|
|---------------------------|-------------------------------|-----------|
|UNKNOWN_ERROR	|unknownError|
|626	|User does not exist|
|658  |Password is invalid	|There is something wrong at Server side|
|616 |invalid arguments|
|632|Brief does not exist|
|633|Brief is removed|
|676| Invalid brief type|
|634|Invalid comment (min 1 and max 500 chars, no html)|
|651|Invalid brief comment reference|
|652|Brief comment does not exist|
|653|Brief comment is removed|
|642|Invalid permissions|
|646|Invalid item count|
|648|Invalid page|
|6002|Invalid return my status value|
|629|Brand does not exist|
|630|Brand is removed|




