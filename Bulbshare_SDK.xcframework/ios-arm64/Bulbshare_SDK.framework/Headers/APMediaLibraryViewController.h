#import <UIKit/UIKit.h>

@protocol APMediaLibraryViewControllerDelegate <NSObject>
@optional
-(void)getSelectedPhoto:(NSMutableArray *)aryPhoto;
- (void)closeRecorderController;
- (void)recordVideo;
- (void)takePhoto;
@end

@import Photos;

@interface APMediaLibraryViewController : UIViewController
{
    UIBarButtonItem *barButtonCamera;
}
@property (weak) id <APMediaLibraryViewControllerDelegate> delegate;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activity;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;
@property (weak, nonatomic) IBOutlet UIButton *albumButton;

@property (weak, nonatomic) IBOutlet UILabel *lblNumberOfPhotoSelected;
@property (weak, nonatomic) NSString *strTitle;
@property (weak, nonatomic) IBOutlet UIView *viewBottom;
@property (nonatomic) BOOL isSegueFromRecorder;
@property (strong, nonatomic) NSMutableArray *arySelectedPhoto;
@property (strong, nonatomic) NSMutableArray *arrImage;
@property (readwrite) int maxCount;
@end

