#import <Foundation/Foundation.h>

@interface NSString (AventaPlus)

+(nonnull NSString*)timeStringForTimeInterval:(NSTimeInterval)timeInterval;

- (NSString*)deviceName;
- (NSString*)platformString;

- (NSString*)remaningTime:(NSDate*)startDate endDate:(NSDate*)endDate;
- (NSString*)dayDiferenceWithMode:(int)mode;
- (NSString*)remaningTimeInMinutes;

+ (NSString *)htmlEntityDecode:(NSString *)string;

- (NSString*)stringByStrippingHTML;
- (NSString*)extractYoutubeIdFromLink;

- (BOOL)isEmpty;
- (BOOL)isValidEmailAddress;

- (NSString *)ZOWMD5String;

@property (nonatomic, readonly, copy) NSString *suffix;

- (NSString *)generateStringWithSuffix:(NSString *)suffix;
- (NSString *)getPureStringWithoutSuffix;
- (BOOL)isVideoSuffix;
- (BOOL)isImageSuffix;

- (NSString*)getShortName;
- (NSString*)getSegmentTextForObject:(int)object andCount:(int)count;

- (NSString*)calculateNumber:(NSInteger)number forMaxNumber:(NSInteger)maxNumber;
- (NSString *)timeFormatConvertToSeconds:(NSTimeInterval)duration cutVideo:(BOOL)cut;

- (NSString*)getCharAtIndex:(int)index;
- (BOOL)htmlTagDetection;
- (NSString*)setNumberLocalisation:(int)number;
- (NSString*)getDateFileName;
- (NSString*)getDateFileNameForUrl;
- (NSString*)birthDateToAge:(NSString*)birthDate;
- (NSString*)ageToBirthDate:(NSString*)age;

- (BOOL)containsEmoji:(NSString *)emoji;
@end
