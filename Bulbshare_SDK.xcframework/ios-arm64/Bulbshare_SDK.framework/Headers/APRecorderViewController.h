//
//  APRecorderViewController.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 18/07/2017.
//  Copyright © 2017 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreMedia/CMTime.h>
#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>
#import <AssetsLibrary/AssetsLibrary.h>

#import "APMediaLibraryViewController.h"
#import "TOCropViewController.h"
//#import "SCRecorder.h"
@class APRecorderViewController;
@protocol APRecorderViewControllerDelegate <NSObject>
- (void)refreshBulbshareFeed:(NSString*)bulbshareRef;
- (void)didTakePhoto:(NSMutableDictionary*)dic;
- (void)didTakeVideo:(NSMutableDictionary*)dic;
@end

@class AVPlayer;
@class AVPlayerDemoPlaybackView, APRecorderViewController;

@interface APRecorderViewController : UIViewController <UINavigationControllerDelegate, UIImagePickerControllerDelegate, APMediaLibraryViewControllerDelegate, TOCropViewControllerDelegate>
{
    AVAssetExportSession *exporter;
}

@property (weak, nonatomic) IBOutlet UIButton *cancelButton;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
//@property (strong, nonatomic) SCRecorder *recorder;
@property (nonatomic) BOOL isRecording;

@property (weak, nonatomic) IBOutlet UIButton *backPhotoButton;
@property (weak, nonatomic) IBOutlet UIButton *continuePhotoButton;

@property(weak) id<APRecorderViewControllerDelegate> delegate;

@end

