//
//  APMediaLibraryHeader.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 22/02/2017.
//  Copyright © 2017 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol APMediaLibraryHeaderDelegate <NSObject>
@optional
-(void)takePhoto;
@end

@interface APMediaLibraryHeader : UICollectionReusableView

@property (weak) id <APMediaLibraryHeaderDelegate> delegate;
@property (weak, nonatomic) IBOutlet UILabel *textlabel;

@end

