#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface UIAlertController (AventaPlus)

+ (void)showAlertWithTitle:(NSString *)title message:(NSString *)message buttonText:(NSString*)buttonText;

@end
