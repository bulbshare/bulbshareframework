//
//  BasePerson.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 1. 09. 15.
//  Copyright (c) 2015 Aventa Plus d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@interface BasePerson : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *displayName;
@property UIImage* avatarImage;
@property (nonatomic, strong) NSString *pictureURL;
@property (nonatomic, strong) NSString *thumbnailPictureURL;
@property (strong, nonatomic) NSString* userref;
@property NSInteger userid;
@property (strong, nonatomic) NSString* fbId;
@property BOOL is_followed_by_me;
@property BOOL is_blocked;
@property BOOL can_unblock;

//Counts
@property NSInteger likesCount;
@property NSInteger followersCount;
@property NSInteger followFriendsCount;
@property NSInteger followBrandsCount;
@property NSString *rewardsCount;

@end
