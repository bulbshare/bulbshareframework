#import <Foundation/Foundation.h>
#import "APConstants.h"
//#import "APConnectionManager.h"
#import <AVFoundation/AVFoundation.h>

@interface APDataController : NSObject

//- (NSMutableDictionary*)parseResponse:(NSData*)data withParseType:(APConnectionParseType)parseType;

- (UIImage *)scaleImage:(UIImage *)image;
- (void)report_memory;
- (void)print_free_memory;
- (void)logout;
- (int)isVideoPortrait:(AVAsset*)asset;
@end


