//
//  BaseBrand.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 1. 09. 15.
//  Copyright (c) 2015 Aventa Plus d.o.o. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
//#import "JoinChannel.h"

@interface BaseBrand : NSObject

@property NSInteger brandid;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *brandRef;
@property (nonatomic, strong) NSString *brandDescription;
@property (nonatomic, strong) NSString *bannerImage;

@property (nonatomic, strong) NSData *primary_color_data;
@property (nonatomic, strong) NSData *secondary_color_data;

@property UIImage* logo;
@property (nonatomic, strong) NSString *pictureURL;
@property (nonatomic, strong) NSString *thumbnailPictureURL;

@property NSInteger briefsCount;
@property NSInteger likesCount;
@property NSInteger followersCount;
@property NSInteger briefCount;

@property BOOL is_followed_by_me;
@property BOOL is_removed;
@property BOOL is_private;
@property BOOL user_has_access;
@property BOOL is_discoverable;
@property BOOL require_screener;

@property (nonatomic, strong) NSString *leftImageURL;
@property (nonatomic, strong) NSString *middleImageURL;
@property (nonatomic, strong) NSString *rightImageURL;
@property (nonatomic, strong) NSString *discover_title;
@property (nonatomic, strong) NSString *discover_about;
@property (nonatomic, strong) NSString *screener_url;
@property (nonatomic, strong) NSString *screener_briefref;

@property (nonatomic, strong) NSMutableArray *briefArray;
//@property (nonatomic, strong) JoinChannel* joinChannel;

@end

