//
//  APMediaPhotoTableViewCell.h
//  Bulbshare
//
//  Created by Smiljan Kerencic on 15/02/2018.
//  Copyright © 2018 Aventa Plus d.o.o. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface APMediaPhotoTableViewCell : UITableViewCell

@property (nonatomic,strong) IBOutlet UIImageView *mainImageView;
@property (nonatomic,strong) IBOutlet UIButton *removeMedia;

@property (nonatomic,strong) IBOutlet UIView *videoProgressView;
@property (nonatomic,strong) IBOutlet UILabel *videoProgressLabel;

@end
