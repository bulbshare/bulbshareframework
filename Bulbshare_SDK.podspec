Pod::Spec.new do |spec|
  spec.name         = 'Bulbshare_SDK'
  spec.version      = '1.10.2'
  spec.license      = "MIT"
  spec.homepage     = 'https://bitbucket.org/bulbshare/bulbshareframework'
  spec.authors      = { 'Smiljan Kerencic' => 'smiljan@bulbshare.com' }
  spec.summary      = 'Bulbshare_SDK Cocoa Pod'
  spec.source       = { :git => 'https://bitbucket.org/bulbshare/bulbshareframework.git', :tag => spec.version }
  spec.vendored_frameworks = "Bulbshare_SDK.xcframework"
  spec.pod_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.user_target_xcconfig = { 'EXCLUDED_ARCHS[sdk=iphonesimulator*]' => 'arm64' }
  spec.platform = :ios
  spec.swift_version = "5.0"
  spec.ios.deployment_target  = '12.0'
spec.dependency "SDWebImage", "~> 5.12.3"
spec.dependency "SwiftLint"
spec.dependency "IQKeyboardManagerSwift"
spec.dependency "KeychainAccess"
spec.dependency "MBProgressHUD"
spec.dependency "Koloda"
spec.dependency "XCDYouTubeKit", "~> 2.15"
spec.dependency "SVProgressHUD"
spec.dependency "JDStatusBarNotification"
spec.dependency "KSToastView", "0.5.7"
spec.dependency "SCRecorder"
spec.dependency "TwitterKit"
spec.dependency "FacebookCore"
spec.dependency "FacebookLogin"
spec.dependency "FacebookShare"
spec.dependency "Alamofire"
spec.dependency "GoogleSignIn", "~> 5.0"
spec.dependency "Firebase/Core"
spec.dependency "Firebase/Auth"
end